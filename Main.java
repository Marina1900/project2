import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите элементы массива: ");
        double[] arr = new double[10];
        for (int i = 0; i < arr.length; i++){
            double num = in.nextDouble();
            arr[i] = num + 0.1 * num;
        }

        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < 9; j++) {
                if (arr[j] < arr[j + 1]) {
                    double bubble = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = bubble;
                }
            }
        }

        System.out.print("Новый массив: ");
        for (double result : arr) {
             System.out.print(result + ", ");
        }
    }
}

