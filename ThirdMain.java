import java.util.Scanner;

public class ThirdMain {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String firstString = in.nextLine();
        System.out.println("Перевернутая строка: " + reflection(firstString));
    }

    public static String reflection(String firstString) {
        String part1, part2;
        int length = firstString.length();
        if (length == 1) {
            return firstString;
        }
        part1 = firstString.substring(0, length / 2);
        part2 = firstString.substring(length / 2, length);
        return reflection(part2) + reflection(part1);
    }
}
